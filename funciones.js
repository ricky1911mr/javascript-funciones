//funciones
function miFuncion(){
    console.log('se va a ejecutar mi funcion')
    return 'esta es mi funcion'
}
miFuncion();
console.log(miFuncion())

// Funciones anonimas

var a =function(){
    return 'funcion anonima'
}
console.log(a())
var b =()=>{
    let c=5
    a();
    return 'funcion array'
}
console.log(a())

var sumar=(a,b)=>{
    return a+b
}
console.log(sumar(2,7))

var array=[3,4]
var array2=['Martes',5]
var json1={
    id:2,
    nombre: 'Ricardo'
}
var json2={
    id:3,
    apellido: 'Paredes'
}


/**
 * 
 * Funcion para Sumar
 * @author Ricardo Paredes
 * @copyright Manticore-Labs
 * @param {object} valor1
 * @param {object} valor2
 * @returns {object}
 */
function sumar(valor1,valor2){
    return valor1+valor2
}

/**
 * Funcion para Restar
 * @author Ricardo Paredes
 * @copyright Manticore-Labs
 * @param {object} valor1
 * @param {object} valor2
 * @returns {object}
 */
function restar(valor1,valor2){
    return valor1-valor2
}

/**
 * Funcion para Multiplicar
 * @author Ricardo Paredes
 * @copyright Manticore-Labs
 * @param {object} valor1
 * @param {object} valor2
 * @returns {object}
 */
function multiplicar(valor1,valor2){
    return valor1*valor2
}
/**
 * Funcion para Dividir
 * @author Ricardo Paredes
 * @copyright Manticore-Labs
 * @param {object} valor1
 * @param {object} valor2
 * @returns {object}
 */
function dividir(valor1,valor2){
    return valor1/valor2
}

var json={
    sumar: (valor1,valor2)=>{
        
    },
    restar: (valor1,valor2)=>{
        return valor1-valor2
    },
    multiplicar: (valor1,valor2)=>{
        return valor1*valor2
    },
    dividir: (valor1,valor2)=>{
        return valor1/valor2
    },

}

console.log('\nSuma')
console.log('Suma numbers:', json.sumar(3,9))
console.log('Suma strings:', json.sumar('Hola', 'mundo'))
console.log('Suma booleans:', json.sumar(true, false))
console.log('Suma arrays:', json.sumar(array, array2))
console.log('Suma nulls:', json.sumar(null, null))
console.log('Suma undefined:', json.sumar(undefined, undefined))
console.log('Suma json:', json.sumar(json1, json2))
console.log('Suma numbers y string:', json.sumar(3, 'mundo'))
console.log('Suma numbers y boolean:', json.sumar(3, false))
console.log('Suma numbers y arrays:', json.sumar(3, array))
console.log('Suma numbers y nulls:', json.sumar(9, null))
console.log('Suma numbers y undefined:', json.sumar(8, undefined))
console.log('Suma numbers y json:', json.sumar(3,json1))
console.log('Suma string y boolean:', json.sumar('Hola',true))
console.log('Suma string y array:', json.sumar('Hola',array))
console.log('Suma string y null:', json.sumar('Hola',null))
console.log('Suma string y undefined:', json.sumar('Hola',undefined))
console.log('Suma string y json:', json.sumar('Hola',json2))
console.log('\nResta')
console.log('Resta numbers:', json.restar(3,9))
console.log('Resta strings:', json.restar('Hola', 'mundo'))
console.log('Resta booleans:', json.restar(true, false))
console.log('Resta arrays:', json.restar(array, array2))
console.log('Resta nulls:', json.restar(null, null))
console.log('Resta undefined:', json.restar(undefined, undefined))
console.log('Resta json:', json.restar(json1, json2))
console.log('Resta numbers y string:', json.restar(3, 'mundo'))
console.log('Resta numbers y boolean:', json.restar(3, false))
console.log('Resta numbers y arrays:', json.restar(3, array))
console.log('Resta numbers y nulls:', json.restar(9, null))
console.log('Resta numbers y undefined:', json.restar(8, undefined))
console.log('Resta numbers y json:', json.restar(3,json1))
console.log('Resta string y boolean:', json.restar('Hola',true))
console.log('Resta string y array:', json.restar('Hola',array))
console.log('Resta string y null:', json.restar('Hola',null))
console.log('Resta string y undefined:', json.restar('Hola',undefined))
console.log('Resta string y json:', json.restar('Hola',json2))
console.log('\nMultiplicacion')
console.log('Multiplica numbers:', json.multiplicar(3,9))
console.log('Multiplica strings:', json.multiplicar('Hola', 'mundo'))
console.log('Multiplica booleans:', json.multiplicar(true, false))
console.log('Multiplica arrays:', json.multiplicar(array, array2))
console.log('Multiplica nulls:', json.multiplicar(null, null))
console.log('Multiplica undefined:', json.multiplicar(undefined, undefined))
console.log('Multiplica json:', json.multiplicar(json1, json2))
console.log('Multiplica numbers y string:', json.multiplicar(3, 'mundo'))
console.log('Multiplica numbers y boolean:', json.multiplicar(3, false))
console.log('Multiplica numbers y arrays:', json.multiplicar(3, array))
console.log('Multiplica numbers y nulls:', json.multiplicar(9, null))
console.log('Multiplica numbers y undefined:', json.multiplicar(8, undefined))
console.log('Multiplica numbers y json:', json.multiplicar(3,json1))
console.log('Multiplica string y boolean:', json.multiplicar('Hola',true))
console.log('Multiplica string y array:', json.multiplicar('Hola',array))
console.log('Multiplica string y null:', json.multiplicar('Hola',null))
console.log('Multiplica string y undefined:', json.multiplicar('Hola',undefined))
console.log('Multiplica string y json:', json.multiplicar('Hola',json2))
console.log('\nDivision')
console.log('Divide numbers:', json.dividir(3,9))
console.log('Divide strings:', json.dividir('Hola', 'mundo'))
console.log('Divide booleans:', json.dividir(true, false))
console.log('Divide arrays:', json.dividir(array, array2))
console.log('Divide nulls:', json.dividir(null, null))
console.log('Divide undefined:', json.dividir(undefined, undefined))
console.log('Divide json:', json.dividir(json1, json2))
console.log('Divide numbers y string:', json.dividir(3, 'mundo'))
console.log('Divide numbers y boolean:', json.dividir(3, false))
console.log('Divide numbers y arrays:', json.dividir(3, array))
console.log('Divide numbers y nulls:', json.dividir(9, null))
console.log('Divide numbers y undefined:', json.dividir(8, undefined))
console.log('Divide numbers y json:', json.dividir(3,json1))
console.log('Divide string y boolean:', json.dividir('Hola',true))
console.log('Divide string y array:', json.dividir('Hola',array))
console.log('Divide string y null:', json.dividir('Hola',null))
console.log('Divide string y null:', json.dividir('Hola',null))
console.log('Divide string y null:', json.dividir('Hola',null))
console.log('Divide string y undefined:', json.dividir('Hola',undefined))
console.log('Divide string y json:', json.dividir('Hola',json2))






